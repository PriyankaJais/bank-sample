About This Application
=====================

The Bank-Sample application fetches information from a sample JSON hosted on dropbox and shows the accounts information on screen.
The application shows the accounts list and allows users to choose the accounts they wish to see on the home screen.
Please consider with the development team / concerned person before customising the application.

Main Features
================

1) Makes a network call and fetched the accounts JSON
1) List all the accounts after parsing the JSON file.
2) On tapping on the Settings button, the view changes and user can select and deselect the accounts they wish to see on home screen
4) Supports all device orientations on iPhone and iPad
5) If the app is launched when the device is not connected to Internet. Then it shows a header on the Top with message 'Device is Offline. Please try again.'
6) AccountTest.swift contains Unit Test to test the Account model.

Frameworks used
======================================

This application uses a custom NetworkUtility framework.

Development Environment
=========================

1) IDE - XCode v8.2.1
2) iOS SDK - v10.2
3) Minimum Deployment Target - iOS v8.0 and later.
4) Dropbox link for the sample JSON : https://www.dropbox.com/s/vfrr8kusct1b50t/response.json?raw=1

Note: Testing done with iPhone 7 simulator.
==========================================================