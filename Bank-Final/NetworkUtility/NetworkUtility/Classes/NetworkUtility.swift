//
//  NetworkUtility.swift
//  NetworkUtility
//  This class provides the functionality for making GET requests.
//  It can be extended to provide functionality for supporting PUT, POST, DELETE as well.
//  Created by Priyanka Jaiswal on 03/08/16.
//  Copyright © 2016 Priyanka Jaiswal. All rights reserved.
//

import Foundation

public class NetworkUtility: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate {
    //Definition for SuccessHandler and FailureHandler
    typealias SuccessHandler = (response: AnyObject?,  statusCode :NSInteger) -> Void
    typealias FailureHandler = (error: NSError) -> Void
    
    var baseURL: NSURL?
    var urlSession: NSURLSession?
    
    /**
     Initializes an instance of Networking with a URL
     @param url An NSURL object
     @return nil
     */
    
    public func initWithURL (url: NSURL!) -> NetworkUtility {
        //Create a defaultSessionConfiguration
        //Should use an ephemeralSessionConfiguration to prevent credentials or other information from getting stored in the cache.db
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        //Create the default header dictionary
        let xHTTPAdditionalHeaders: [NSObject: AnyObject] = [Constants.ContentType: Constants.ApplicationJson]
        sessionConfig.HTTPAdditionalHeaders = xHTTPAdditionalHeaders
        //Time out for request in secs
        sessionConfig.timeoutIntervalForRequest = Constants.TimeoutIntervalForRequest
        //Time out for response in secs
        sessionConfig.timeoutIntervalForResource = Constants.TimeoutIntervalForResource
        //Maximum number of simultanous persistent connections
        sessionConfig.HTTPMaximumConnectionsPerHost = Constants.MaximumConnectionsPerHost
        //Create the session from the sessionConfig
        self.urlSession = NSURLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        //Set the URL
        self.baseURL = url
        return self
    }
    
    /**
     Creates and runs an `NSURLSessionDataTask` for a `GET` request.
     @param success A block object to be executed when the task finishes successfully. This block has no return value and takes as argument the response object created by the client response serializer.
     @param failure A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes as argument the error describing the network or parsing error that occurred.
     @return nil
     */
    
    public func GET (success: ((responseObject: AnyObject?, statusCode: NSInteger) -> Void),
                     failure: ((error: NSError) -> Void)) -> NSURLSessionDataTask? {
        //Create the NSMutableURLRequest
        let mutableRequest: NSMutableURLRequest! = self.createRequest()
        //Perform the network request
        return self.performNetworkRequest(mutableRequest, successHandler: success, failureHandler: failure)
    }
    
    /**
     Removes `NSURLSessionDataTask`.
     @param dataTask the NSURLSessionDataTask that was earlier returned to the consumer
     @return nil.
     */
    
    public func cancelTask (dataTask : NSURLSessionDataTask){
        dataTask.cancel()
    }
    
    /**
     Suspends `NSURLSessionDataTask`.
     @param dataTask the NSURLSessionDataTask that was earlier returned to the consumer
     @return nil.
     */
    public func suspendTask (dataTask : NSURLSessionDataTask){
        dataTask.suspend()
    }
    
    /**
     Resumes `NSURLSessionDataTask`.
     @param dataTask the NSURLSessionDataTask that was earlier returned to the consumer
     @return nil.
     */
    public func resumeTask (dataTask : NSURLSessionDataTask){
        dataTask.resume()
    }
    
    /**
     Get the status of `NSURLSessionDataTask`.
     @param dataTask the NSURLSessionDataTask that was earlier returned to the consumer
     @return NSURLSessionTaskState which defines the status.
     */
    public func statusOfTask (dataTask : NSURLSessionDataTask) -> NSURLSessionTaskState{
        return dataTask.state
    }

    //Method to create NSMutableRequest with the URL string
    
    func createRequest() -> NSMutableURLRequest {
        //Create an instance of NSMutableURLRequest
        let mutableRequest:NSMutableURLRequest = NSMutableURLRequest()
        //Set the HHTPBody
        mutableRequest.HTTPBody = nil
        //Set the URL
        mutableRequest.URL = self.baseURL
        //Set the HTTPMethod as Get
        mutableRequest.HTTPMethod = Constants.Get as String
        return mutableRequest
    }
    
    //Method that performs the network operation
    
    func performNetworkRequest(request: NSMutableURLRequest, successHandler: SuccessHandler, failureHandler: FailureHandler)-> NSURLSessionDataTask? {
        var dataTask = NSURLSessionDataTask?()
        dataTask = (self.urlSession?.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            if let error = error {
                //Call the failureHandler with the error
                failureHandler(error: error )
            }
            else {
                //Get the response as NSHTTPURLResponse
                let internalResponse: NSHTTPURLResponse = response as! NSHTTPURLResponse
                //Extract the header fields
                let headers: NSDictionary =  (response as! NSHTTPURLResponse).allHeaderFields
                do {
                    //Try to get the response dictionary from the data
                    if let jsonObject  = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                        //Making a dictionary of reponse headers and response body
                        let resultDataDictionary: NSDictionary = NSDictionary(objects: [headers, jsonObject], forKeys: [Constants.ResponseHeaders, Constants.ResponseObject])
                        //Pass the resultDataDictionary back to calling application
                        successHandler(response: resultDataDictionary, statusCode: internalResponse.statusCode);
                    }
                }
                catch let caught as NSError {
                    //Call the failureHandler with the parsing error
                    failureHandler(error: caught)
                }
            }
        }))!
        
        //Start the dataTask
        dataTask!.resume()
        return dataTask
    }
}

