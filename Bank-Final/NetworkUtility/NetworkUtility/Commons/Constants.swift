//
//  Constants.swift
//  Networking
//
//  Created by Priyanka Jaiswal on 03/08/16.
//  Copyright © 2016 Priyanka Jaiswal. All rights reserved.
//

import Foundation

struct Constants {
    static let ContentType: String = "Content-Type"
    static let ApplicationJson: String = "application/json"
    static let Get: String = "GET"
    static let SuccessStatus: Int = 200
    static let TimeoutIntervalForRequest: NSTimeInterval = 30.0
    static let TimeoutIntervalForResource: NSTimeInterval = 60.0
    static let MaximumConnectionsPerHost: Int = 1
    static let ResponseHeaders: String = "responseHeaders"
    static let ResponseObject: String = "responseObject"
}