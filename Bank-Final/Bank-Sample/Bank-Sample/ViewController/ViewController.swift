//
//  ViewController.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 03/08/16.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var accountsTableView: UITableView!
    
    @IBOutlet weak var errorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var errorLabel: UILabel!
    
    let sharedBankDataManager = AccountDataManager.sharedManager

    var dataSource = NSMutableArray()
    var shouldAllowSelection = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpUI()
        self.fetchAccounts()
        self.setUpReachabilityObserver()
        
    }
}

// MARK: - UITableViewDataSource methods
extension ViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRowsInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("accountsInfoCell", forIndexPath: indexPath) as! AccountsTableViewCell
        let numberOfRowsInPreviousSection = self.numberOfRowsInSection(indexPath.section-1)
        let account = self.dataSource.objectAtIndex(indexPath.row + numberOfRowsInPreviousSection) as? Account
        account?.showButtonValueChangedBlock = {
            account?.toggleShowAndHide()
        }
        cell.accountInfo = account;
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        //Get the number of unique account types
        return sharedBankDataManager.uniqueAccountType().count
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier("sectionViewCell") as! SectionHeaderTableViewCell
        cell.sectionTitle = sharedBankDataManager.uniqueAccountType().objectAtIndex(section) as? String
        if tableView.allowsSelection {
            //If on Settings screen, dont show currency
            cell.sectionCurrency = Constants.EmptyStrBank
        }
        else {
            //Else show currency
            cell.sectionCurrency = Constants.defaultCurrency
        }
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
}

// MARK: - UITableViewDelegate methods
extension ViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  80.0
    }
    
    
// MARK: - Button action methods
    func settBanksButtonPressed() {
        self.shouldAllowSelection = true
        self.accountsTableView.allowsSelection = !self.accountsTableView.allowsSelection
        //Modify the accounts list and reload the tableview
        sharedBankDataManager.toggleShowBalance()
        self.reloadTableView()
        
        self.navigationItem.rightBarButtonItems?.removeAll()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(ViewController.doneButtonPressed))
        self.navigationItem.rightBarButtonItems?.insert(doneButton, atIndex: 0)
    }
    
    func doneButtonPressed() {
        self.shouldAllowSelection = false
        self.accountsTableView.allowsSelection = !self.accountsTableView.allowsSelection
        //Change the accounts list and reload the tableview
        sharedBankDataManager.toggleShowBalance()
        self.reloadTableView()
        //Add the settBanks bar button to the navigation bar
        self.addSettingsRightBarButton()
        

    }
    
    // MARK: - Custom methods
    
    func fetchAccounts() -> Void {
        //Fetch the account information
        AccountAPIClient.sharedClient.fetchAccountsInformation { [weak self] (success, error) in
            if(success) { //The accounts list was populated
                if let weakSelf = self {
                    weakSelf.addSettingsRightBarButton()
                    weakSelf.errorLabelHeight.constant = 0
                    weakSelf.updateViewConstraints()
                    weakSelf.reloadTableView()
                }
            }
            else {
                //Some Http status code other than 200 was returned
                if let error = error {
                    if let weakSelf = self {
                        if error.isErrorDueToNoNetworkConnection() {
                            weakSelf.reachabilityChanged()
                        }
                        
                    }
                }
            }
        }
    }
    
    func setUpReachabilityObserver() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.reachabilityChanged), name: kReachabilityChangedNotification, object: nil)
    }
    
    func reachabilityChanged() {
        if OfflineManager.sharedManager.isOffline {
            self.navigationItem.rightBarButtonItems?.removeAll()
            self.errorLabel.text = ErrorDomainDeviceIsOffline
            self.errorLabelHeight.constant = 20
            self.updateViewConstraints()
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), {[weak self] () -> Void in
                if let weakSelf = self {
                    weakSelf.errorLabelHeight.constant = 0
                    weakSelf.updateViewConstraints()
                    
                }
                })
        } else {
            self.errorLabelHeight.constant = 0
            self.updateViewConstraints()
        }
    }
    
    func updateDataSource() {
        let sectionNames = sharedBankDataManager.uniqueAccountType()
        self.dataSource.removeAllObjects()
        for sectionName in sectionNames {
            if self.shouldAllowSelection {
                self.dataSource.addObjectsFromArray(sharedBankDataManager.accountForType(sectionName as! String) as [AnyObject])
            } else {
                self.dataSource.addObjectsFromArray(sharedBankDataManager.accountsToBeDisplayedOnHomeScreenForType(sectionName as! String) as [AnyObject])
            }
        }
    }
    
    func setUpUI() {
        self.accountsTableView.allowsSelection = false
        self.title = Constants.AccountsListViewTitle
        self.errorLabelHeight.constant = 0
        self.updateViewConstraints()
    }
    
    func numberOfRowsInSection(section:Int)->Int {
        if section < 0 {
            return 0
        }
        
        if self.shouldAllowSelection {
            //If on Settings screen, show all accounts
            return sharedBankDataManager.accountForType((sharedBankDataManager.uniqueAccountType().objectAtIndex(section) as? String)!).count
        }
        else {
            //Get the accounts with the account type as the section account type and for which showOnHomeScreen is true
            return sharedBankDataManager.accountsToBeDisplayedOnHomeScreenForType((sharedBankDataManager.uniqueAccountType().objectAtIndex(section) as? String)!).count
        }
    }
    
    func reloadTableView() {
        self.updateDataSource()
        self.accountsTableView.reloadData()
    }
    
    func addSettingsRightBarButton() {
        //Add custom right bar button
        let button = UIButton(type: UIButtonType.Custom)
        button.setBackgroundImage(UIImage(named: "Settings"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ViewController.settBanksButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems?.removeAll()
        //Add button to navigationbar
        self.navigationItem.rightBarButtonItems?.insert(barButton, atIndex:  0)

    }
    
}
