//
//  NSError+Additions.swift
//  ING-Sample
//
//  Created by Priyanka Jaiswal on 05/08/16.
//  Copyright © 2016 sogeti. All rights reserved.
//

import Foundation

let ErrorDomainDeviceIsOffline = "Device is Offline. Please try again."
let ErrorDomainInvalidReponse = "Invalid response"

extension NSError {
    
    class func noInternetConnection() -> NSError {
        return NSError(domain: ErrorDomainDeviceIsOffline, code: -1009, userInfo: nil)
    }
    
    func isErrorDueToNoNetworkConnection()->Bool {
        return self.domain == ErrorDomainDeviceIsOffline
    }
    
    class func invalidResponse() -> NSError {
        return NSError(domain: ErrorDomainInvalidReponse, code: -1009, userInfo: nil)
    }
}