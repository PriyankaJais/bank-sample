//
//  AccountAPIClient.swift
//  Bank-Sample
//  This class acts as a service manager and interacts with the NetworkUtility, parses the json and populates data in DataManager
//  Created by Priyanka Jaiswal on 04/08/16.
//

import Foundation
import NetworkUtility

public class AccountAPIClient {
    typealias AccountAPICompletionHandler = (success:Bool, error: NSError?) -> ()
    
    //Constructor.
    class var sharedClient :AccountAPIClient {
        struct Singleton {
            static let instance = AccountAPIClient()
        }
        
        return Singleton.instance
    }
    
    let networkUtility = NetworkUtility()
    
    func fetchAccountsInformation(completionHandler: AccountAPICompletionHandler!)-> NSURLSessionDataTask? {
        
        if(OfflineManager.sharedManager.isOffline) {
            if completionHandler != nil {
                completionHandler(success: false, error: NSError.noInternetConnection())
            }
            return nil
        }
        
        var task : NSURLSessionDataTask!
        
        //Make the network call using NetworkUtility and fetch the JSON response
        if let url = NSURL(string: Constants.AccountsURL) {
            networkUtility.initWithURL(url)
            task = networkUtility.GET({ (responseObject: AnyObject?, statusCode: NSInteger) -> Void in
                if (statusCode == Constants.HttpSuccess) {
                    //Parse the response and populate the accountsList in Data manager
                    let sharedBankDataManager = AccountDataManager.sharedManager
                    
                    if let responseDictionary = responseObject as? Dictionary<String, AnyObject>, let response = responseDictionary[Constants.ResponseObject]![Constants.Response] as? Dictionary<String, AnyObject>, accountsDictionary = response[Constants.Accounts] as? [Dictionary<String, String>] {
                        for account in accountsDictionary {
                            sharedBankDataManager.addAccount(Account(dictionary: account))
                        }
                    }
                    if task.state != .Canceling {
                        dispatch_async (dispatch_get_main_queue()) {
                            if completionHandler != nil {
                                completionHandler(success: true, error: nil)
                            }
                        }
                    }
                }
                else {
                    if task.state != .Canceling {
                        dispatch_async (dispatch_get_main_queue()) {
                            if completionHandler != nil {
                                completionHandler(success: false, error: NSError.invalidResponse())
                            }
                        }
                    }
                }
                },
                                      failure: { (error: NSError) -> Void in
            })
        }
        
        
        return task
    }
}
