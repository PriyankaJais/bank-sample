//
//  AccountDataManager..swift
//  Bank-Sample
//  This class manages the accounts list and returns the appropriate records to be shown on the screen
//  Created by Priyanka Jaiswal on 04/08/16.
//

import Foundation

public class AccountDataManager {
    
    //Constructor.
    class var sharedManager :AccountDataManager {
        struct Singleton {
            static let instance = AccountDataManager()
        }
        
        return Singleton.instance
    }
    
    private var accounts  = NSMutableArray()
    
    //MARK:- Convenience public Methods
    func addAccount(account:Account) -> Void {
        self.accounts.addObject(account)
    }
    
    func resetAccounts(account:Account) -> Void {
        self.accounts.removeAllObjects()
    }
    
    func allAccounts() -> NSMutableArray {
        return self.accounts;
    }
    
    func uniqueAccountType()-> NSArray {
        return (self.accounts as NSMutableArray).valueForKeyPath("@distinctUnionOfObjects.accountType") as! NSArray
    }
    
    func accountForType(type: String)->NSArray {
        return  self.accounts.filteredArrayUsingPredicate(NSPredicate(format: "accountType contains[cd] %@", type))
    }
    
    func accountsToBeDisplayedOnHomeScreenForType(type: String)->NSArray {
        return  self.accounts.filteredArrayUsingPredicate(NSPredicate(format: "showInHomeScreen == %@ && accountType contains[cd] %@", true, type))
    }
    
    func accountsToBeDisplayedOnHomeScreen() -> NSArray {
        return  self.accounts.filteredArrayUsingPredicate(NSPredicate(format: "showInHomeScreen == %@", true))
    }
    
    func toggleShowBalance() -> Void {
        for account in accounts {
            (account as! Account).toggleShowBalance()
        }
    }
    
}
