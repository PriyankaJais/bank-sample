//
//  OfflineManager.swift
//  Bank-Sample
//  Created by Priyanka Jaiswal on 04/08/16.
//

import Foundation

private let OfflineManagerSharedInstance = OfflineManager()

class OfflineManager: NSObject {
    class var sharedManager : OfflineManager {
        return OfflineManagerSharedInstance
    }
    
    var connection: Reachability {
        return Reachability.reachabilityForInternetConnection()
    }
    
    var isOffline : Bool {
        if self.connection.currentReachabilityStatus() == NetworkStatus.NotReachable {
            return true
        }
        else {
            return false
        }
    }
    
    func reachabilityChanged(notification : NSNotification?) {
        NSNotificationCenter.defaultCenter().postNotificationName(kReachabilityChangedNotification, object: nil)
    }

    override init() {
        super.init()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OfflineManager.reachabilityChanged(_:)), name: kReachabilityChangedNotification, object: nil)
        self.connection.startNotifier()
    }
}
