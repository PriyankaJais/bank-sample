//
//  Account.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 03/08/16.
//

import Foundation
typealias ShowButtonValueChangedBlock = () -> ()

class Account : NSObject {
    var accountName: String?
    var accountNumber: String?
    var accountType: String?
    var accountIcon: String?
    var accountCurrency: String?
    var accountBalance: String?
    var cellStyle: Constants.AccountCellStyle?
    var showInHomeScreen = true
    var showButtonValueChangedBlock : ShowButtonValueChangedBlock!
    
    init(dictionary : Dictionary<String, String>) {
        if let accountName = dictionary[Constants.AccountName] {
            if accountName == Constants.EmptyStrBank {
                self.accountName = dictionary[Constants.Alias]
            }
            else {
                self.accountName = accountName
            }
        }
        if let accountNumber = dictionary[Constants.AccountNumber] {
            self.accountNumber = accountNumber
        }
        if let accountType = dictionary[Constants.AccountTypeKey] {
            self.accountType = accountType
        }
        if let accountCurrency = dictionary[Constants.AccountCurrency] {
            self.accountCurrency = accountCurrency
        }
        if let accountBalance = dictionary[Constants.AccountBalanceInCents] {
            
            let accountBalanceInEur = Float(accountBalance as String)!/100
            self.accountBalance = String(accountBalanceInEur)
        }
        
        self.cellStyle = Constants.AccountCellStyle.ShowBalance
    }
    
    func toggleShowAndHide() {
        self.showInHomeScreen = !self.showInHomeScreen
        
    }
    
    func toggleShowBalance() {
        switch self.cellStyle! {
        case  .HideBalance:
            self.cellStyle = Constants.AccountCellStyle.ShowBalance
            break
        case .ShowBalance:
            self.cellStyle = Constants.AccountCellStyle.HideBalance
        }
    }
    

}
