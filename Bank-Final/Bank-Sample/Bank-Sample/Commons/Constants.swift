//
//  Constants.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 03/08/16.
//

import Foundation

struct Constants {
    enum AccountCellStyle: Int {
        case HideBalance = 0
        case ShowBalance = 1
    }
    static let PaymentAccount = "PAYMENT"
    static let SavBanksAccount = "SAVBank"
    static let AccountsListViewTitle = "Accounts"
    static let AccountsURL = "https://www.dropbox.com/s/vfrr8kusct1b50t/response.json?raw=1"
    static let HttpSuccess = 200
    static let ResponseObject = "responseObject"
    static let Response = "response"
    static let Accounts = "accounts"
    static let AccountName = "accountName"
    static let AccountBalanceInCents = "accountBalanceInCents"
    static let AccountNumber = "accountNumber"
    static let AccountCurrency = "accountCurrency"
    static let AccountTypeKey = "accountType"
    static let defaultCurrency = "EUR"
    static let Alias = "alias"
    static let EmptyStrBank = ""
    static let UserInfo = "userInfo"
}
