//
//  SectionHeaderTableViewCell.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 03/08/16.
//

import UIKit

class SectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet private weak var accountType: UILabel!
    @IBOutlet private weak var currency: UILabel!

    var sectionTitle: String? {
        didSet {
            self.accountType.text = sectionTitle
          }
    }

    var sectionCurrency: String? {
        didSet {
            self.currency.text = sectionCurrency
        }
    }

}
