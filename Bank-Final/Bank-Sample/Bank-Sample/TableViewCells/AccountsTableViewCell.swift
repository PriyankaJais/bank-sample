//
//  AccountsTableViewCell.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 03/08/16.
//

import UIKit

class AccountsTableViewCell: UITableViewCell {
    @IBOutlet private weak var customerName: UILabel!
    @IBOutlet private weak var accountBalance: UILabel!
    @IBOutlet private weak var accountName: UILabel!
    @IBOutlet private weak var showButton: UIButton!
    
    @IBOutlet private weak var showButtonWidth: NSLayoutConstraint!
    @IBOutlet private weak var tapGestureRecognizer : UITapGestureRecognizer!
    
    @IBOutlet private weak var accountImageWidth: NSLayoutConstraint!
    
    var accountInfo: Account? {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        if self.gestureRecognizers?.count <= 0 {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AccountsTableViewCell.toggleCheckMark))
            self.addGestureRecognizer(tapGesture)
        }
    }
    
    
     private func updateUI() {
        self.customerName.text = accountInfo?.accountName
        self.accountBalance.text = accountInfo?.accountBalance
        self.accountName.text = accountInfo?.accountNumber
        
        if(accountInfo?.cellStyle == Constants.AccountCellStyle.ShowBalance) {
            
            self.showButtonWidth.constant = 0
            self.updateConstraints()
            self.accountBalance.hidden = false
        }
        else {
            self.showButtonWidth.constant = 30
            self.updateConstraints()
            self.accountBalance.hidden = true
        }
        self.setNeedsLayout()
    }
    
    
    @objc private func toggleCheckMark() {
        if accountInfo?.cellStyle == Constants.AccountCellStyle.HideBalance {
            self.showButton.hidden = !self.showButton.hidden
            if self.accountInfo!.showButtonValueChangedBlock != nil {
                self.accountInfo!.showButtonValueChangedBlock()
            }
        }
    }


}
