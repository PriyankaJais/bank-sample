//
//  AccountTest.swift
//  Bank-Sample
//
//  Created by Priyanka Jaiswal on 05/08/16.
//

import XCTest

class AccountTest: XCTestCase {
    
    let accountDictionary = [
                             Constants.AccountName : "AccountNameValue",
                             Constants.AccountNumber : "1234587",
                             Constants.AccountTypeKey : "SAVBankS",
                             Constants.AccountCurrency : "EUR",
                             Constants.AccountBalanceInCents : "10000"
    ]
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /*
     * We passBank a dictionary with all the fields filled.
     * Expected result is that all the fields should have valid data.
     */
    func testAccountInstantiationAndParsBank() -> Void {
        //Setup
        let accountInfo : Account!
        
        //Execute
        accountInfo = Account(dictionary: accountDictionary)
        accountInfo.cellStyle = Constants.AccountCellStyle.ShowBalance
        
        //Verify
        XCTAssert(accountInfo != nil, "Instantiation of accountInfo Failed")
        
        XCTAssert(accountInfo.accountName != nil, "AssignBank value to accountName field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountName?.isEmpty)!, "AssignBank value to accountName field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountNumber != nil, "AssignBank value to accountNumber field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountNumber?.isEmpty)!, "AssignBank value to accountNumber field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountType != nil, "AssignBank value to accountType field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountType?.isEmpty)!, "AssignBank value to accountType field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountIcon == nil, "accountIcon should be Nil.")
        
        XCTAssert(accountInfo.accountCurrency != nil, "AssignBank value to accountCurrency field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountCurrency?.isEmpty)!, "AssignBank value to accountCurrency field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountBalance != nil, "AssignBank value to accountBalance field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountBalance?.isEmpty)!, "AssignBank value to accountBalance field of Account Failed. Value is Empty.")

        XCTAssert(accountInfo.cellStyle == Constants.AccountCellStyle.ShowBalance, "AssignBank value to cellStyle field of Account Failed.")
        
    }
    
    
    /*
     * We are not passBank AccountName and AccountNumber in the dictionary.
     * Expected result is that these two fields should be initialised to nil. Rest of the fields should have valid data
     */
    func testParsBankWithPartialDataParsBank() -> Void {
        //Setup
        let accountDictionary = [
            Constants.AccountTypeKey : "SAVBankS",
            Constants.AccountCurrency : "EUR",
            Constants.AccountBalanceInCents : "10000"
        ]
        let accountInfo : Account!
        
        //Execute
        accountInfo = Account(dictionary: accountDictionary)
        accountInfo.cellStyle = Constants.AccountCellStyle.ShowBalance
        
        //Verify
        XCTAssert(accountInfo != nil, "Instantiation of accountInfo Failed")
        
        XCTAssert(accountInfo.accountName == nil, "AssignBank value to accountName field of Account Failed. Variable is Nil.")
        XCTAssert(accountInfo.accountNumber == nil, "AssignBank value to accountNumber field of Account Failed. Variable is Nil.")
        
        
        XCTAssert(accountInfo.accountType != nil, "AssignBank value to accountType field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountType?.isEmpty)!, "AssignBank value to accountType field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountIcon == nil, "accountIcon should be Nil.")
        
        XCTAssert(accountInfo.accountCurrency != nil, "AssignBank value to accountCurrency field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountCurrency?.isEmpty)!, "AssignBank value to accountCurrency field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.accountBalance != nil, "AssignBank value to accountBalance field of Account Failed. Variable is Nil.")
        XCTAssert(!(accountInfo.accountBalance?.isEmpty)!, "AssignBank value to accountBalance field of Account Failed. Value is Empty.")
        
        XCTAssert(accountInfo.cellStyle == Constants.AccountCellStyle.ShowBalance, "AssignBank value to cellStyle field of Account Failed.")
        
    }
    
    /*
     * We instantiate an Account Object. The default initial value of 'showInHomeScreen' is true. Then we call the testToggleShowAndHide method.
     * Expected result is that value of 'showInHomeScreen' should toggle to false.
     */
    func testToggleShowAndHide() {
        //Setup
        let accountInfo = Account(dictionary: accountDictionary)
        
        //Execute
        accountInfo.toggleShowAndHide()
        
        //Verify
        XCTAssert(!accountInfo.showInHomeScreen, "toggleShowAndHide() method of Account class Failed.")
    }
    
    
    
    
    
}
